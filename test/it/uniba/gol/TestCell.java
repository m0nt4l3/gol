package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;




public class TestCell {

	@Test
	public void cellShouldBeLive() throws Exception {
		Cell cell = new Cell(0, 0, true);
		assertTrue(cell.isAlive());
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldBeRaiseException() throws Exception {
		new Cell(-1, 0, true);
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldBeRaiseException2() throws Exception {
		new Cell(0, -1, true);
	}
	
	@Test
	public void cellSHouldBeDead() throws Exception {
		Cell cell = new Cell(0, 0, true);
		cell.setAlive(false);
		assertFalse(cell.isAlive());
	}
	
	@Test
	public void cellSHouldReturnX() throws Exception {
		Cell cell = new Cell(5, 0, true);
		assertEquals(5, cell.getX());
	}
	
	@Test
	public void cellSHouldReturnY() throws Exception {
		Cell cell = new Cell(0, 5, true);
		assertEquals(5, cell.getY());
	}
	
	@Test 
	public void cellShouldAliveNeighbords() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertEquals(3, cells[1][1].getNumberOfAliveNeighbords());
		
		
		
	}
	
	@Test
	public void aliveCellWithThreeNeighborsShouldSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertTrue(cells[1][1].willSurvive());
		
		
		
	}
	
	@Test
	public void aliveCellWithTwoNeighborsShouldSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertTrue(cells[1][1].willSurvive());
		
		
		
	}
	
	@Test
	public void aliveCellNotSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, false);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, false);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertFalse(cells[1][1].willSurvive());
		
		
		
	}
	
	@Test
	public void aliveCellWithOneNeighborsShouldSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, true);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, false);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertFalse(cells[1][1].willSurvive());
		
		
		
	}

	
	@Test
	public void aliveCellWithLessTwoNeighborsShouldSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, false);
		cells[0][1] = new Cell(0, 1, true);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, false);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertTrue(cells[1][1].willDie());
		
		
	}
	
	@Test
	public void aliveCellWithMoreThreeNeighborsShouldSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, true);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertTrue(cells[1][1].willDie());
		
		
	}
	
	@Test
	public void aliveCellWithMoreTwoLessThreeNeighborsShouldSurvive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertFalse(cells[1][1].willDie());
		
		
	}
	
	@Test
	public void deathCellWillDeath() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, false);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertFalse(cells[1][1].willDie());
		
		
	}
	
	
	@Test
	public void deathCellWithMoreThreeNeighborsWillRevive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, false);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, true);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertTrue(cells[1][1].willRevive());
		
		
	}
	
	@Test
	public void deathCellWithLessThreeNeighborsWillRevive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, false);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertFalse(cells[1][1].willRevive());
		
		
	}
	
	@Test
	public void aliveCellWithLessThreeNeighborsWillRevive() throws Exception {
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		cells[1][1].setNumberOfAliveNeighbords(cells);
		
		assertFalse(cells[1][1].willRevive());
		
		
	}
	
	@Test
	public void neighbordCheck()throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		
		
		assertFalse(cells[0][0].isNeighboard(cells[2][2]));
		
	}
	

	
	@Test
	public void neighbordCheckNeighboardsX()throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		assertTrue(cells[0][0].isNeighboard(cells[1][0]));
		
		
	}

	@Test
	public void neighbordCheckNeighboardsY()throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		assertTrue(cells[0][0].isNeighboard(cells[0][1]));
		
		
	}
	
	@Test
	public void neighbordCheckNeighboardsNotNear()throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0, 0, true);
		cells[0][1] = new Cell(0, 1, false);
		cells[0][2] = new Cell(0, 2, false);
		
		cells[1][0] = new Cell(1, 0, false);
		cells[1][1] = new Cell(1, 1, true);
		cells[1][2] = new Cell(1, 2, false);
		
		cells[2][0] = new Cell(2, 0, true);
		cells[2][1] = new Cell(2, 1, false);
		cells[2][2] = new Cell(2, 2, false);
		
		assertFalse(cells[0][0].isNeighboard(cells[0][2]));
		
		
	}
}
